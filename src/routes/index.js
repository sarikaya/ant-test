import React from "react";
import { Route, Router, Switch } from "react-router";
import { createBrowserHistory } from "history";
import Header from "../components/Header";
import Home from "../screens/Home";

const Routes = props => {
  const history = createBrowserHistory(props);
  return (
    <div>
      <Header />
      <Router history={history}>
        <Switch>
          <Route exact path="/" component={Home} />
        </Switch>
      </Router>
    </div>
  );
};
export default Routes;
