import { actionType } from "./actions";
const initialState = {
  alert: null,
  authModalVisibility: false,
  verifySMSModalVisibility: false,
  loading: false
};

export default (state = initialState, action) => {
  switch (action.type) {
    case actionType.START:
      return {
        ...state,
        loading: true,
        alert: null
      };
    case actionType.LOGIN:
      return {
        ...state,
        mainText: "LOGGED IN!",
        loading: false,
        alert: null
      };
    case actionType.SIGNUP:
      return {
        ...state,
        mainText: "SIGNED UP!",
        loading: false,
        alert: null
      };
    case actionType.UNAUTH_ERR:
      return {
        ...state,
        mainText: null,
        loading: false,
        alert: {
          message: "Authentication Failed !",
          type: "err"
        }
      };
    case actionType.SWITCH_AUTH_MODAL:
      return {
        ...state,
        authModalVisibility: !state.authModalVisibility,
        authTab: action.payload,
        alert: null
      };
    case actionType.SWITCH_SMS_MODAL:
      return {
        ...state,
        verifySMSModalVisibility: !state.verifySMSModalVisibility,
        alert: null
      };
    default:
      return state;
  }
};
