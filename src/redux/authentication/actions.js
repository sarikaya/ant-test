export const actionType = {
  LOGIN: "LOGIN",
  SIGNUP: "SIGNUP",
  START: "START",
  UNAUTH_ERR: "UNAUTH_ERR",
  SWITCH_AUTH_MODAL: "SWITCH_AUTH_MODAL",
  SWITCH_SMS_MODAL: "SWITCH_SMS_MODAL"
};

export const login = params => async dispatch => {
  dispatch({
    type: actionType.START
  });
  await new Promise(resolve => setTimeout(resolve, 2500));
  if (params.loginUser === "admin" && params.loginPass === "123456") {
    dispatch({
      type: actionType.LOGIN
    });
  } else {
    dispatch({
      type: actionType.UNAUTH_ERR
    });
  }
};

export const signup = params => async dispatch => {
  dispatch({
    type: actionType.START
  });
  await new Promise(resolve => setTimeout(resolve, 2500));
  if (params.code === "1234") {
    dispatch({
      type: actionType.SIGNUP
    });
  } else {
    dispatch({
      type: actionType.UNAUTH_ERR
    });
  }
};

export const switchAuthModal = params => async dispatch => {
  dispatch({
    type: actionType.SWITCH_AUTH_MODAL,
    payload: params
  });
};

export const switchSMSModal = () => async dispatch => {
  dispatch({
    type: actionType.SWITCH_SMS_MODAL
  });
};
