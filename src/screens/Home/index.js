import React, { Component } from "react";
import { Button } from "antd";
import "./index.css";
import AuthModal from "../../components/AuthModal";
import VerifySMSModal from "../../components/VerifySMSModal";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import {
  switchAuthModal,
  switchSMSModal,
  login,
  signup
} from "../../redux/authentication/actions";

class Home extends Component {
  handleLogin = async credentials => {
    let { switchAuthModal, login } = this.props;

    await login(credentials);
    switchAuthModal();
  };

  handleSignup = credentials => {
    let { switchAuthModal, switchSMSModal } = this.props;
    this.setState({
      credentials
    });
    switchAuthModal();
    switchSMSModal();
  };

  handleVerification = async params => {
    let { signup } = this.props;
    let { credentials } = this.state;
    await signup({
      ...credentials,
      ...params
    });
  };

  render() {
    let {
      authModalVisibility,
      verifySMSModalVisibility,
      switchAuthModal,
      switchSMSModal,
      mainText
    } = this.props;
    return (
      <div className="App">
        <div>{mainText}</div>
        <AuthModal
          visible={authModalVisibility}
          onCancel={() => switchAuthModal()}
          handleLogin={this.handleLogin}
          handleSignup={this.handleSignup}
        />
        <VerifySMSModal
          visible={verifySMSModalVisibility}
          onCancel={() => switchSMSModal()}
          onSubmit={this.handleVerification}
        />
      </div>
    );
  }
}

const mapStateToProps = state => ({
  authModalVisibility: state.authentication
    ? state.authentication.authModalVisibility
    : false,
  verifySMSModalVisibility: state.authentication
    ? state.authentication.verifySMSModalVisibility
    : false,
  mainText: state.authentication ? state.authentication.mainText : null
});

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      switchAuthModal,
      switchSMSModal,
      login,
      signup
    },
    dispatch
  );
export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Home);
