import React, { Component } from "react";
import { Modal, Tabs, Form, Button, Input, Icon, Spin } from "antd";
import { connect } from "react-redux";

class AuthModal extends Component {
  constructor(props) {
    super(props);
    this.state = {
      activeTab: null
    };
  }
  componentDidMount() {
    this.initState();
  }
  initState = () => {
    this.setState({
      loginUser: null,
      loginPass: null,
      signupUser: null,
      signupPass: null,
      email: null
    });
  };
  onCancel = () => {
    this.setState({
      activeTab: null
    });
    this.props.onCancel();
  };
  onInputChange = (e, name) => {
    this.setState({
      [name]: e.target.value
    });
  };
  handleLogin = () => {
    this.props.handleLogin(this.state);
    this.initState();
  };

  handleSignup = () => {
    this.props.handleSignup(this.state);
    this.initState();
  };
  render() {
    let { authTab, loading } = this.props;
    let {
      activeTab,
      loginUser,
      loginPass,
      signupUser,
      signupPass,
      email
    } = this.state;
    const TabPane = Tabs.TabPane;
    return (
      <Modal
        title={
          <div style={{ textAlign: "center" }}>
            <Icon type="gitlab" />
            Join RoboDuels today
          </div>
        }
        footer={null}
        visible={this.props.visible}
        onCancel={this.onCancel}
      >
        {loading ? (
          <div style={{ textAlign: "center" }}>
            <Spin size="large" />
          </div>
        ) : (
          <Tabs
            activeKey={activeTab || authTab}
            onChange={key => this.setState({ activeTab: key })}
          >
            <TabPane tab="Log In" key="login">
              <Form>
                <Form.Item style={{ marginBottom: 0 }} label="Username">
                  <Input
                    onChange={e => this.onInputChange(e, "loginUser")}
                    value={loginUser}
                    placeholder="admin"
                  />
                </Form.Item>
                <Form.Item style={{ marginBottom: 0 }} label="Password">
                  <Input.Password
                    onChange={e => this.onInputChange(e, "loginPass")}
                    value={loginPass}
                    placeholder="123456"
                  />
                  <a>Trouble logging in?</a>
                </Form.Item>
                <Form.Item>
                  <Button type="primary" block onClick={this.handleLogin}>
                    Login
                  </Button>
                </Form.Item>
              </Form>
            </TabPane>
            <TabPane tab="Sign Up" key="signup">
              <Form>
                <Form.Item style={{ marginBottom: 0 }} label="Username">
                  <Input
                    onChange={e => this.onInputChange(e, "signupUser")}
                    value={signupUser}
                  />
                </Form.Item>
                <div>
                  This is the name people will know you by on RoboDuels. You can
                  always change it later.
                </div>
                <Form.Item style={{ marginBottom: 0 }} label="Password">
                  <Input.Password
                    onChange={e => this.onInputChange(e, "signupPass")}
                    value={signupPass}
                  />
                </Form.Item>
                <Form.Item label="Email">
                  <Input
                    onChange={e => this.onInputChange(e, "email")}
                    value={email}
                  />
                </Form.Item>
                <div style={{ textAlign: "center" }}>
                  By clicking Sign Up, you are indicated that you have read and
                  agree to the <a>Terms of Service</a> and <a>Privacy Policy</a>
                </div>
                <Form.Item>
                  <Button type="primary" block onClick={this.handleSignup}>
                    Sign Up
                  </Button>
                </Form.Item>
                <div style={{ textAlign: "center", fontWeight: "bold" }}>
                  Already have an account?{" "}
                  <a onClick={() => this.setState({ activeTab: "login" })}>
                    Login
                  </a>
                </div>
              </Form>
            </TabPane>
          </Tabs>
        )}
      </Modal>
    );
  }
}

const mapStateToProps = state => ({
  authTab: state.authentication ? state.authentication.authTab : null,
  loading: state.authentication ? state.authentication.loading : false
});

export default connect(mapStateToProps)(AuthModal);
