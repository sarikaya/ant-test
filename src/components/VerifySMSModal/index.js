import React, { Component } from "react";
import { Modal, Select, Form, Button, Input, Icon, Spin } from "antd";
import { connect } from "react-redux";

class VerifySMSModal extends Component {
  constructor(props) {
    super(props);
    this.state = {
      current: 0
    };
  }
  componentDidMount() {
    this.initState();
  }
  initState = () => {
    this.setState({
      current: 0,
      phone: null,
      code: null
    });
  };
  next = () => {
    let { current } = this.state;
    if (current === 1) {
      this.props.onSubmit(this.state);
      this.initState();
    }
    this.setState({ current: current + 1 });
  };

  prev = () => {
    let { current } = this.state;
    this.setState({ current: current - 1 });
  };

  onInputChange = (e, name) => {
    this.setState({
      [name]: e.target.value
    });
  };

  onCancel = () => {
    this.initState();
    this.props.onCancel();
  };

  render() {
    let { current, phone, code } = this.state;
    let { loading, alert } = this.props;
    const { getFieldDecorator } = this.props.form;
    const { Option } = Select;
    const prefixSelector = getFieldDecorator("prefix", {
      initialValue: "1"
    })(
      <Select style={{ width: 70 }}>
        <Option value="1">+1</Option>
        <Option value="2">+2</Option>
      </Select>
    );
    const steps = [
      {
        content: (
          <Form>
            <div style={{ fontSize: 20 }}>Enter Your Phone Number</div>
            <div>We will send you a one time verification code.</div>
            <Form.Item label="Phone Number">
              {getFieldDecorator("phone", {
                rules: [
                  { required: true, message: "Please input your phone number!" }
                ]
              })(
                <Input
                  value={phone}
                  onChange={e => this.onInputChange(e, "phone")}
                  addonBefore={prefixSelector}
                  style={{ width: "100%" }}
                />
              )}
            </Form.Item>
            <div>
              To verify your account we'll need to confirm your phone number.
              This will only take a few seconds.
            </div>
            <Form.Item>
              <Button type="primary" block onClick={() => this.next()}>
                Next
              </Button>
            </Form.Item>
          </Form>
        )
      },
      {
        content: (
          <Form>
            <div style={{ fontSize: 20 }}>Enter Your Code</div>
            <div>
              We just send a text message to {phone} with a code for you to
              enter here.
            </div>
            <Form.Item label="Code">
              <Input
                value={code}
                placeholder="1234"
                onChange={e => this.onInputChange(e, "code")}
              />
            </Form.Item>
            <div>This can take a few minutes.</div>
            <Form.Item>
              <div style={{ display: "flex" }}>
                <Button block onClick={() => this.prev()}>
                  Back
                </Button>
                <Button block type="primary" block onClick={() => this.next()}>
                  Next
                </Button>
              </div>
            </Form.Item>
          </Form>
        )
      },
      {
        content: (
          <div>
            {alert ? (
              <div
                style={{
                  fontSize: 20,
                  justifyContent: "center",
                  display: "flex",
                  alignItems: "center"
                }}
              >
                <Icon
                  type="close-circle"
                  theme="filled"
                  style={{ fontSize: 40, color: "red" }}
                />{" "}
                Verification Failed
              </div>
            ) : (
              <div
                style={{
                  fontSize: 20,
                  justifyContent: "center",
                  display: "flex",
                  alignItems: "center"
                }}
              >
                <Icon
                  type="check-circle"
                  theme="filled"
                  style={{ fontSize: 40, color: "green" }}
                />{" "}
                Identity Verified
              </div>
            )}
            <Button
              type="primary"
              icon="gitlab"
              block
              style={{ marginTop: 20 }}
              onClick={this.onCancel}
            >
              To Dashboard
            </Button>
          </div>
        )
      }
    ];

    return (
      <Modal
        title={
          <div style={{ textAlign: "center" }}>
            <Icon type="gitlab" />
            Verify SMS
          </div>
        }
        footer={null}
        visible={this.props.visible}
        onCancel={this.onCancel}
      >
        {loading ? (
          <div style={{ textAlign: "center" }}>
            <Spin size="large" />
          </div>
        ) : (
          <div>
            <div>{steps[current].content}</div>
          </div>
        )}
      </Modal>
    );
  }
}
const VerifySMSForm = Form.create({ name: "register" })(VerifySMSModal);

const mapStateToProps = state => ({
  alert: state.authentication ? state.authentication.alert : null,
  loading: state.authentication ? state.authentication.loading : null
});

export default connect(mapStateToProps)(VerifySMSForm);
