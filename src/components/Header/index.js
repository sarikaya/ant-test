import React, { Component } from "react";
import { Layout, Icon, Button, Row, Col } from "antd";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import { switchAuthModal } from "../../redux/authentication/actions";

class Header extends Component {
  render() {
    let { switchAuthModal } = this.props;
    const { Header } = Layout;
    return (
      <Header style={{ background: "#fff", padding: 0 }}>
        <Row type="flex" justify="space-between">
          <Col>
            <Icon
              style={{
                marginLeft: 10,
                fontSize: 18
              }}
              type="gitlab"
            />
            LOGO
          </Col>
          <Col>
            <Button onClick={() => switchAuthModal("login")}>Log In</Button>
            <Button onClick={() => switchAuthModal("signup")} type="link">
              Sign Up
            </Button>
          </Col>
        </Row>
      </Header>
    );
  }
}

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      switchAuthModal
    },
    dispatch
  );
export default connect(
  null,
  mapDispatchToProps
)(Header);
